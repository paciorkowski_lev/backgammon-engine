Welcome to the Persian Backgammon Engine Project! The goal of this project is to discover optimal play in this particular variant of backgammon, commonly played today in Baku, Azerbaijan where it is called "Narde".

To read the rules of the game, please visit  https://bkgm.com/variants/Narde.html  (not written by the author of this project) for a concise overview. The rules can also be found in the Engine_Core module of this project.

NOTE: There is one minor variation in the exact rules used for this project. One of the rules in the website linked above is:

            "You must use both numbers of a roll if possible, or all four numbers in the case of doubles.
            If you can play one number but not both, you must play the higher one."
            
However for this project, if you can play one number but not both, you can play either one. This shouldn't be expected to materially impact gameplay as this situation is an infrequent occurrence. In practice, when I play casual games, this rule is not strictly enforced.


As of 04/19/2020, there are 9 separate modules:

    1) general_functions.py         Holds general purpose functions that are frequently used in numerous other modules, and exists primarily to avoid circular imports
    
    2) graphics.py                  Deals with primitive graphics generation in turtle. Not the primary area of development as of this update.
    
    3) move_finding.py              Generates list of legal moves given a board state, player and dice roll. Includes all game logic. No outstanding bugs, but a secondary priority will be rigorous testing to make sure it functions with 100% accuracy. Currently there is only high confidence of accuracy.
    
    4) rating_tasks.py              Handles the rating of matches. Details on the rating system are in the comments. Not fully operational yet (but mostly operational).
    
    5) debugging.py                 Ad-hoc testing for resolving bugs. There are not currently any known or suspected outstanding bugs, but full rigorous testing hasn't been done yet.
    
    6) heuristics.py                Defines and computes all heuristics for the evaluation of a given board state from a given player's point of view. Currently a high priority area of development. Right now, each individual heuristic is a number score - it is computed with a developer-defined function that takes some quantifiable element of the board state as an input. At the moment, the functional form is fixed but there are various parameters in that function that are being optimized to maximize performance of the Engine.
   
    7) Engine_Core.py               Core functions for defining engines, which differ in the heuristics that they use.
   
    8) human_playing.py             Module for playing human vs. Engine matches. Not too sophisticated; the main area of development is in Engine vs. Engine matches because we can play many test games very quickly there.
   
    9) cpu_playing.py               Module for Engine vs. Engine matches. Completely indpendent of the human_playing module.
    
There are also two ancillary text files that are used by the rating_tasks module:

    a) log.txt                      A comprehensive log of all rated matches played. Will be obsolete soon once data is moved to SQL tables.
    b) ratings.txt                  List of all current Engines and their ratings. Also will be obsolete soon once data is moved to SQL tables.
    
As of 04/19/2020, the primary area of development is in modules 6, 7, and 9

****** UPDATE: 12/10/2020 - Adding an algorithm to optimize the evaluation function used by Smart_Cyrus: ******

One module has been added, namely:
    10) optimizer_SmartCyrus.py

This is a process for finding a theoretical optimal evaluation function for the Smart_Cyrus Engine. Extensive step-by-step details are provided in the code. A brief summary is given here.

Smart_Cyrus decides its chosen move by computing an "evaluation" of every legal move from a given board state with a given dice roll. The move with the highest evaluation is selected as its top choice. This is a strictly deterministic process: for any particular board state and dice roll, Smart_Cyrus can _**always**_ be counted on to pick the same move (provided it uses the same exact evaluation function, of course), with no known exception.

Its evaluation function is composed of multiple sub-functions that calculate various heuristics on a position to estimate its favorability. These sub-functions each have multiple parameters that can be adjusted. Tweaking these parameters directly changes how Smart_Cyrus evaluates positions, which in turn directly influences its playing strength.

I start by assuming that there exists some optimal set of parameters that yields the "strongest" version of Smart_Cyrus. The goal is to find this optimal set, or at least the best local optimal set possible within a reasonable amount of time. This turns out to be a tricky problem of "black box optimization", where the objective function has no known algebraic form.

In fact, it is debatable what the objective function even is! The goal is to find the set of parameters {p} that maximizes the "playing strength" of Smart_Cyrus; however, there is no independent definition of "playing strength". In theory, the objective function would be something that you put {p} into and then get out some measure of "playing strength", but it is not apparent how this should be defined. So in essence, it is not possible to directly measure O({p}), BUT, given two sets of parameters, {p1} and {p2}, it is possible to determine if O({p1}) >/=/< O({p2}). This can be done by simulating a match between two versions of Smart_Cyrus, one using {p1} for its evaluation function parameters and the other using {p2}. If one of the versions wins by a significant point margin, then we can declare that set of parameters to be superior to the other.

This is essentially how my first attempt at an optimization algorithm for this problem works. It starts at some defined initial {p0}, and then has rules for choosing other sets of {p} to test against {p0}. There are decision rules based on hypothesis tests for deciding if one set {p1} is superior, worse, or equivalent to any other set {p2}. The process stops after a defined amount of tests have completed without having found a superior {p} to the currently best known {p}.

A very complex process, and I am not entirely sure what to expect from it - can it find any reasonable improvements for the evaluation function?

Later, I will implement a neural network to find optimal ways of evaluating a position, which I think would be a more efficient approach. Until then, it will be interesting to see how good a version of Smart_Cyrus this algorithm can produce. At the very least, this is a good exercise in a difficult optimization problem.  















