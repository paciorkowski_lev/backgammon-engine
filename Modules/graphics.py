"""
All code dealing with rudimentary graphics generation in turtle
Very primitive, graphics not the primary purpose of this Engine
Something to improve on later
Author: Lev Paciorkowski
Last edited: 04/19/2020
"""
from general_functions import *

# draws the triangles on the board; assumes starts by facing east
def draw_triangle():
    t.down()
    angle1 = m.degrees(m.atan(4))
    angle2 = (90 - angle1) * 2
    side = m.sqrt(8606.25)
    t.forward(45)
    t.left(180 - angle1)
    t.forward(side)
    t.left(180 - angle2)
    t.forward(side)
    t.left(180 - angle1)


# Function to draw what the physical board itself looks like
# board itself has buffer of 50 on all sides; ranges from X(50, 590) and Y(50, 590)
# All functions will always END the turtle facing east with pen UP, for consistency
def draw_board():
    t.setworldcoordinates(0, 0, 640, 640)
    t.speed(0)
    t.pensize(2)
    t.up()
    t.goto(50,50)
    t.down()
    for i in range(2):
        t.forward(540)
        t.left(90)
        t.forward(540)
        t.left(90)
    t.up()
    t.goto(320,50)
    t.left(90)
    t.pensize(1)
    t.down()
    t.forward(540)
    t.up()
    t.goto(50,320)
    t.down()
    t.right(90)
    t.forward(540)
    t.up()
    t.goto(50,50)
    for i in range(12):
        draw_triangle()
        t.up()
        t.forward(45)
    t.up()
    t.goto(590,590)
    t.left(180)
    for i in range(12):
        draw_triangle()
        t.up()
        t.forward(45)
    t.left(180)


# Function to draw pieces, given color, quantity and location on the board
# White pieces are just black circles; black pieces are circles with a black fill of radius 9
def draw_pieces(color, qty, location):
    if qty == 0:
        pass
    else:
        filler = "white" if color == "W" else "black"
        t.fillcolor(filler)
        if location <= 11:
            x_cord = 72.5 + location*45
            y_cord = 50
            t.goto(x_cord, y_cord)
            for _ in range(qty):
                t.down()
                t.begin_fill()
                t.circle(radius = 9)
                t.end_fill()
                t.up()
                y_cord = y_cord + 18
                t.goto(x_cord, y_cord)
        else:
            x_cord = 567.5 - (location - 12)*45
            y_cord = 590
            t.goto(x_cord, y_cord)
            for _ in range(qty):
                t.left(180)
                t.down()
                t.begin_fill()
                t.circle(radius = 9)
                t.end_fill()
                t.up()
                y_cord = y_cord - 18
                t.goto(x_cord, y_cord)
                t.right(180)


# This uses the function above to draw all the pieces on the board, given two length-24 lists of board and qty
# Parameter <board> denotes piece color and <qty> denotes number of pieces in that space
def set_board_position(board, qty):
    for i in range(24):
        draw_pieces(color = board[i], qty = qty[i], location = i)
