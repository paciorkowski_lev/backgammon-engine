"""
Algorithm for finding an optimal parameter configuration on the engine Smart_Cyrus

Smart_Cyrus is an engine that takes 13 parameters, which control the functions used to evaluate heuristics
    "advancement" and "vulnerability". Three parameters are passed into the advancement function, while the 
    remaining ten are passed to the vulnerability function. These parameters directly influence how
    Smart_Cyrus evaluates positions, which in turn directly influences its playing strength. I hypothesize
    that there exists some optimal set of parameters (configuration) which will yield superior "playing strength"
    (to be defined below) relative to all other possible configurations.
    
The process for finding a (local) optimum "set of parameters" ("configuration") is as follows:
    1) Choose an intial configuration, labeled C_0. I will arbitrarily choose the configuration where each
        parameter takes a value of 1.
    2) Define the set of "neighboring" configurations as the ones reachable by tweaking any one parameter by a
        constant delta in either the positive or the negative direction. Since there are 13 parameters, this set
        of neighbors will have 26 members. I am arbitrarily choosing a delta of 0.05.
    3) Randomly choose a neighbor from this set, and label the new configuration C_1.
    4) Play n games, where Player A is Smart_Cyrus using C_1 and Player B is Smart_Cyrus using C_0. I am choosing
        n = 100; ideally this should be a large enough n for the Central Limit Theorem to apply for the descriptive
        statistic calculated in (6).
    5) Record the following measures:
        w1 = # games Player A wins by 1 point
        w2 = # games Player A wins by 2 points
        l1 = # games Player B wins by 1 point
        l2 = # games Player B wins by 2 points
    6) Calculate the descriptive statistic xbar and sample standard deviation s:
        xbar = (w1 + 2*w2 - l1 - 2*l2)/n
        s = sqrt((w1*(1-xbar)^2 + w2*(2-xbar)^2 + l1(-1-xbar)^2 + l2(-2-xbar)^2) / (n-1))
        
        So in essence, what we have done is conceptualized our population as being the set of all possible games
        that could be played between the two versions of Smart_Cyrus. This population is incalculably large, but
        finite. For any given board state, dice roll, and parameter configuration, it will always be possible to
        reproduce the same top move chosen by the Engine, and there are a finite number of games that can be
        represented as sequences of board states.
        
        We then take a random sample of size n from this population of games by actually playing n games between
        the two versions of Smart_Cyrus. This is sampling with replacement, but the odds of repeating the same
        exact game within any reasonable number of games n is negligible.
        
        Finally, for each of the n observations (games), we can measure how many points Player A scored, which
        is a discrete random variable with possible values [-2, -1, 1, 2]. The descriptive statistic xbar is the
        sample average number of points Player A scores in a game. For a large enough n, this sampling distribution
        should be approximately normal, so we can apply a t-test for hypothesis testing.
        
    7) Define the population mean mu as the mean points scored by Player A against Player B with the assigned
        parameter configurations C_1 and C_0, across all possible games that could be played. This is how I
        define the notion of "playing strength".
        
        Execute the following hypothesis test:
            H0: mu = 0 (the configurations are equivalent in strength)
            Ha: mu > 0 (The new configuration used by Player A is stronger)
            
        The test statistic will be:
            t = xbar / (s/sqrt(n))
            
        Compute the p-value p for this t-stat on v = n-1 degrees of freedom.
        
        If p < alpha, go to step 8. Otherwise, go to step 9.
        
    8) If p < alpha, operate under the assumption that the new configuration C_1 is superior to C_0. In this
        case, do the following:
            A) Adopt C_1 and redefine it as the new C_0
            B) Construct the new set of neighboring configurations {C_n} from the new C_0 adopted in 8A.
            C) If every member of {C_n} has already been "visited" earlier in this process - i.e. was tested by
                playing n games at some previous iteration - TERMINATE THE PROCESS. Return the new C_0 as the optimum
                configuration found.
            D) Otherwise, find the member of {C_n} that represents the tweaking of the SAME parameter by delta
                in the SAME direction that was used to go from the old C_0 to the old C_1/new C_0. If this neighbor
                has already been visited, then choose any random unvisited neighbor. Denote the chosen neighbor
                as the new C_1.
            E) Go back and execute from step 4.
    
    9) If p >= alpha, operate under the assumption that the new configuration C_1 is not superior to C_0. In this
        case, do the following:
            A) Pick any random unvisited neighbor from {C_n}. If all neighbors have been visited, TERMINATE
                THE PROCESS. Return C_0 as the optimum configuration found.
                **Please note that we are still using the SAME set of neighbors from the old C_0 here; we DO NOT
                find a new set of neighbors in this step.
            B) Otherwise, if you have an unvisited neighbor, denote it C_1 and go back to execute from step 4.
    
    
    Additional notes:
        - for all runs of this optimization algorithm, I am recording logs of each configuration visited and results
            of statistical tests in local SQL tables.

Author: Lev Paciorkowski
Last edited: 12/10/2020
"""


from cpu_playing import *
from scipy.stats import t
from datetime import datetime
import MySQLdb

# For a given delta and config, returns a list of neighboring configurations
def find_neighbors(delta, config):
    neighbors = []
    for i in range(2*len(config)):
        neighbor = config[:]
        param = i//2
        if i % 2 == 0:
            neighbor[param] += delta
        else:
            neighbor[param] -= delta
        neighbors.append(neighbor)
    return neighbors


# This function will query a SQL database to see if a given configuration has been visited already
# Returns true if visited IN THE CURRENT RUN, false otherwise
# if visited, also returns the configID
def config_visited(config, run, cur):
    x, y, z, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10 = config
    
    query_params = {'runid': run, 'x': x, 'y': y, 'z': z, 'a1': a1, 'a2': a2, 'a3': a3, 'a4': a4, 'a5': a5, \
                    'a6': a6, 'a7': a7, 'a8': a8, 'a9': a9, 'a10': a10}
    
    select_stmt = ("SELECT CONFIG_ID FROM configs WHERE "
                   "RUN_ID = %(runid)s AND "
                   "X = %(x)s AND "
                   "Y = %(y)s AND "
                   "Z = %(z)s AND "
                   "A1 = %(a1)s AND "
                   "A2 = %(a2)s AND "
                   "A3 = %(a3)s AND "
                   "A4 = %(a4)s AND "
                   "A5 = %(a5)s AND "
                   "A6 = %(a6)s AND "
                   "A7 = %(a7)s AND "
                   "A8 = %(a8)s AND "
                   "A9 = %(a9)s AND "
                   "A10 = %(a10)s")
            
    # if this returns results, then the config has been visited this run
    if cur.execute(select_stmt, query_params) != 0:
        result = cur.fetchall()
        return True, result[0][0] # True and configID
    else:
        # executed query returns no results
        return False, None

# Takes a connection and returns the next configID -- the next highest unused number
# Also adds the given config as a row entry into configs
def get_next_configID(run_id, config, cur):
    x, y, z, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10 = config
    
    select_stmt = ("SELECT MAX(cast(CONFIG_ID as unsigned)) FROM configs")

    cur.execute(select_stmt)
    result = cur.fetchall()
    maximum = result[0][0]
    if maximum is None:
        identifier = 1
    else:
        identifier = int(maximum) + 1
    
    insert_stmt = ("INSERT INTO configs(CONFIG_ID, RUN_ID, X, Y, Z, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10) "
                   "VALUES (%(config_id)s, "
                   "%(run_id)s, "
                   "%(x)s, "
                   "%(y)s, "
                   "%(z)s, "
                   "%(a1)s, "
                   "%(a2)s, "
                   "%(a3)s, "
                   "%(a4)s, "
                   "%(a5)s, "
                   "%(a6)s, "
                   "%(a7)s, "
                   "%(a8)s, "
                   "%(a9)s, "
                   "%(a10)s)")
    
    insert_params = {'config_id': str(identifier), 'run_id': run_id, 'x': x, 'y': y, 'z': z, 'a1': a1, 'a2': a2, \
                    'a3': a3, 'a4': a4, 'a5': a5, 'a6': a6, 'a7': a7, 'a8': a8, 'a9': a9, 'a10': a10}
    
    cur.execute(insert_stmt, insert_params)
    
    return str(identifier)

# analogous function for returning the next runID
def get_next_runID(cur):
    select_stmt = ("SELECT MAX(cast(RUN_ID as unsigned)) FROM runs")

    cur.execute(select_stmt)
    result = cur.fetchall()
    maximum = result[0][0]
    if maximum is None:
        identifier = 1
    else:
        identifier = int(maximum) + 1
    return str(identifier)
    
    


def optimize_SmartCyrus(n, delta, alpha):    
    # Initialize SQL connection --> this connection should be able to see updated changes made by itself
    cnxn = MySQLdb.connect(host='localhost',
                           user='backgammon',
                           passwd='smartcyrus',
                           db='backgammon')
    cur = cnxn.cursor()
    # cur.execute('SELECT * FROM runs') -- format for SQL command execution
    
    run_start = datetime.now()
    
    # Create a run ID, an up-to-8 character string
    run_id = get_next_runID(cur)
    
    # Step 1 - define initial configuration, i.e. starting point
    config_0 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]  # 13 parameters all = 1
    
    # Step 2 - define set of neighbors
    neighbors = find_neighbors(delta, config_0)

    # Step 3 - randomly choose a neighbor --> save the seed for future reference
    seed = r.randint(0, len(neighbors)-1)
    config_1 = neighbors[seed]
    
    step_number = 1
    print("The run id is: ", run_id)
    
    while True:
        print("On step number: ", step_number)
        current_time = datetime.now()
        elapsed = current_time - run_start
        raw_time = (elapsed.seconds + elapsed.microseconds/1000000)/60
        print("Elapsed time (min): ", round(raw_time, 2))
        # Step 4 - play n games between "Player A" using config_1 and "Player B" using config_0
        # i.e. Player A is the "new" player and Player B is the "old" player
        # Both are versions of the Smart_Cyrus Engine
        
        step_start = datetime.now() # record start time for this step
        # Player 1 (A) should always be using config_1 (new) and Player 2 (B) should always be using config_0 (old)
        scores, pt_margins = cpu_match('Smart_Cyrus', 'Smart_Cyrus', n, False, 1, True, config_1, config_0)
        step_end = datetime.now() # used to calculate seconds/game for measure of operating speed
        elapsed = step_end - step_start
        raw_time = elapsed.seconds + elapsed.microseconds/1000000
        sec_per_game = raw_time/n
        
        # Step 5 - Record the four measures: w1, w2, l1, l2
        w1, w2, l1, l2 = pt_margins[0]
        
        # Step 6 - Calculate xbar and s
        xbar = (w1 + 2*w2 - l1 - 2*l2)/n
        s = m.sqrt((w1*(1-xbar)**2 + w2*(2-xbar)**2 + l1*(-1-xbar)**2 + l2*(-2-xbar)**2) / (n-1))
    
        # Step 7 - Execute hypothesis test; xbar is point estimate for mu
        # H0: mu = 0
        # Ha: mu > 0
        
        # Calculate test statistic
        t_stat = xbar / (s/m.sqrt(n))
    
        # p-value for upper tailed test is 1 - cdf(t)
        rv = t(df=n-1, loc=0, scale=1)
        p_value = 1 - rv.cdf(t_stat)
        
        decision = 'CONFIG_1' if p_value < alpha else 'CONFIG_0'
        
        # First step of pushing results to SQL --> register the tested configurations and assign them an ID
        visited_0, config_0_id = config_visited(config_0, run_id, cur) # This should always return True and an ID
        if not visited_0: # This should NEVER happen UNLESS on the very first step
            config_0_id = get_next_configID(run_id, config_0, cur)
            cnxn.commit()
        
        # Config 1 should by definition ALWAYS be unvisited; we are never retesting a previously visited config in
        # this process
        config_1_id = get_next_configID(run_id, config_1, cur)
        cnxn.commit()
        
        # Write to the main optimization log here
        insert_stmt = ("INSERT INTO optimization_log_sc(RUN_ID, STEP_NO, CONFIG_ID_0, CONFIG_ID_1, W1,"
                                                        "W2, L1, L2, XBAR, S, T_STAT, P_VAL, DECISION,"
                                                        "SEC_GAME, DTM) "
        "VALUES (%(run_id)s, "
        "%(step_no)s, "
        "%(config_id_0)s, "
        "%(config_id_1)s, "
        "%(w1)s, "
        "%(w2)s, "
        "%(l1)s, "
        "%(l2)s, "
        "%(xbar)s, "
        "%(s)s, "
        "%(t_stat)s, "
        "%(p_val)s, "
        "%(decision)s, "
        "%(sec_game)s, "
        "%(dtm)s)")
        
        insert_params = {'run_id': run_id, 'step_no': step_number, 'config_id_0': config_0_id, \
                         'config_id_1': config_1_id, 'w1': w1, 'w2': w2, 'l1': l1, 'l2': l2, \
                         'xbar': xbar, 's': s, 't_stat': t_stat, 'p_val': p_value, 'decision': decision, \
                         'sec_game': sec_per_game, 'dtm': datetime.now()}
        cur.execute(insert_stmt, insert_params)
        cnxn.commit()
        
        
        if p_value < alpha:
            # Step 8 - operate under the assumption now that config_1 is superior to config_0
            # 8A - adopt config_1 as the NEW config_0 ... will be done officially later, but for now
            # declare it to be a temporary config_2, because we will need config_0 for reference
            config_2 = config_1[:]
            # 8B - construct new set of neighboring configs
            neighbors = find_neighbors(delta, config_2)
            # 8D - Do this before 8C, since most times it will be more efficient
            # Find the neighbor that corresponds to tweaking the same param in the same direction
            proposed_config = neighbors[seed] # check if this proposed config has been visited before
            visited, identity = config_visited(proposed_config, run_id, cur)
            if visited:
                # pick any random unvisited config then
                true_call_indices = [] # list of indices of unvisited configs
                for i in range(len(neighbors)):
                    visited, identity = config_visited(neighbors[i], run_id, cur)
                    if not visited:
                        true_call_indices.append(i)
                if true_call_indices == []:
                    # TERMINATE THE PROCESS --> all neighbors have been visited
                    # config_optimal = config_2
                    config_optimal_id = config_1_id
                    break
                else:
                    seed = true_call_indices[r.randint(0, len(true_call_indices)-1)]
                    config_next = neighbors[seed]
                    # Now make the config switch official
                    config_0 = config_2
                    config_1 = config_next
                    # and return to the top of the while loop
                    step_number += 1
            else: # this proposed config, going in the same "direction" is unvisited
                config_0 = config_2
                config_1 = proposed_config
                # return to the top of the while loop
                step_number += 1
        else: # p_value >= alpha
            # Step 9 - operate under the assumption that config_1 is not superior to config_0
            # 9A - go back to the original set of neighbors from config_0 and pick any random unvisited one
            true_call_indices = []
            for i in range(len(neighbors)):
                visited, identity = config_visited(neighbors[i], run_id, cur)
                if not visited:
                    true_call_indices.append(i)
            if true_call_indices == []:
                # TERMINATE THE PROCESS --> all neighbors have been visited
                # config_optimal = config_0 # return this as the optimal config
                config_optimal_id = config_0_id
                break
            else:
                seed = true_call_indices[r.randint(0, len(true_call_indices)-1)]
                config_next = neighbors[seed]
                # Now make the config switch official
                config_1 = config_next
                # config_0 REMAINS THE SAME ***
                # return to the top of the while loop
                step_number += 1
        
    # Exited the while loop
    run_end = datetime.now()
    insert_stmt = ("INSERT INTO runs(RUN_ID, START_DTM, END_DTM, N, DELTA, ALPHA, OPTIMAL_CONFIG) "
                   "VALUES (%(run_id)s, "
                   "%(start_dtm)s, "
                   "%(end_dtm)s, "
                   "%(n)s, "
                   "%(delta)s, "
                   "%(alpha)s, "
                   "%(optimal_config)s)")
    
    insert_params = {'run_id': run_id, 'start_dtm': run_start, 'end_dtm': run_end, 'n': n, 'delta': delta, \
                     'alpha': alpha, 'optimal_config': config_optimal_id}
    cur.execute(insert_stmt, insert_params)
    cnxn.commit()
    print("RUN CONCLUDED SUCCESSFULLY")
    print("Optimal config ID: ", config_optimal_id)
    cnxn.close()
    



def main():
    optimize_SmartCyrus(n=500, delta=0.75, alpha=0.05)
    print("Finished first optimization")
    
    optimize_SmartCyrus(n=300, delta=1, alpha=0.05)
    print("Finished second optimization")
    
    optimize_SmartCyrus(n=750, delta=0.25, alpha=0.025)
    print("Finished third optimization")
    
    


main()















