"""
Module to hold general purpose functions that may be used in multiple other modules
Added to avoid circular import problems

Author: Lev Paciorkowski
Last udpated: 12/10/2020
"""

import random as r
import turtle as t
import math as m
from dataclasses import dataclass

# EXPORTS TO graphics.py, move_finding.py, rating_tasks.py


# Function to roll two six-sided dice and returns the result
def dice_roll():
    roll_1 = r.randint(1, 6)
    roll_2 = r.randint(1, 6)
    return (roll_1, roll_2)

# Function for initiating board and pieces as lists
def init():
    board = []
    for i in range(26):
        board += [0]
    board[0] = "W"
    board[12] = "B"
    qty = []
    for i in range(26):
        qty += [0]
    qty[0] = 15
    qty[12] = 15
    return (board, qty)

