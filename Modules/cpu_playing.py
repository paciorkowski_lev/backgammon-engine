"""
Module for handling Engine vs. Engine matches. This is completely independent from the human_playing module.

Author: Lev Paciorkowski
Last updated: 12/09/2020
"""
from Engine_Core import *
# Function to execute commputer matches -- may not be accountability for illegal moves if they happen
# For simplicity, the player who goes first will be chosen randomly each game --> can edit this later
# param auto is a boolean --> for the optimization process this is TRUE to avoid any manual steps like user input
# Returns match score --> can add additional statistics for improved functionality later
# As of 03/24/2020 -->  supported players: Random Play, Cyrus 1.0, Cyrus 1.1, Cyrus 1.2, Smart_Cyrus (04/17/2020)
def cpu_match(player_1, player_2, match_length, details, sets, auto = False, sc_config_1 = None, sc_config_2 = None):
    if not auto:
        if player_1 == 'Smart_Cyrus':
            x_1 = float(input("x (player 1): "))
            y_1 = float(input("y (player 1): "))
            z_1 = float(input("z (player 1): "))
            a1_1 = float(input("a1 (player 1): "))
            a2_1 = float(input("a2 (player 1): "))
            a3_1 = float(input("a3 (player 1): "))
            a4_1 = float(input("a4 (player 1): "))
            a5_1 = float(input("a5 (player 1): "))
            a6_1 = float(input("a6 (player 1): "))
            a7_1 = float(input("a7 (player 1): "))
            a8_1 = float(input("a8 (player 1): "))
            a9_1 = float(input("a9 (player 1): "))
            a10_1 = float(input("a10 (player 1): "))
        if player_2 == 'Smart_Cyrus':
            x_2 = float(input("x (player 2): "))
            y_2 = float(input("y (player 2): "))
            z_2 = float(input("z (player 2): "))
            a1_2 = float(input("a1 (player 2): "))
            a2_2 = float(input("a2 (player 2): "))
            a3_2 = float(input("a3 (player 2): "))
            a4_2 = float(input("a4 (player 2): "))
            a5_2 = float(input("a5 (player 2): "))
            a6_2 = float(input("a6 (player 2): "))
            a7_2 = float(input("a7 (player 2): "))
            a8_2 = float(input("a8 (player 2): "))
            a9_2 = float(input("a9 (player 2): "))
            a10_2 = float(input("a10 (player 2): "))
    if auto:        # for plugging in test configs from the optimizer
        if player_1 == 'Smart_Cyrus':
            x_1, y_1, z_1, a1_1, a2_1, a3_1, a4_1, a5_1, a6_1, a7_1, a8_1, a9_1, a10_1 = sc_config_1
        if player_2 == 'Smart_Cyrus':
            x_2, y_2, z_2, a1_2, a2_2, a3_2, a4_2, a5_2, a6_2, a7_2, a8_2, a9_2, a10_2 = sc_config_2
    scores = []
    pt_margins = []
    for j in range(sets):
        if not auto:
            print("Sets Completed:", j)
            print("Cumulative scores:", scores)
        player_1_score = 0
        player_2_score = 0
        player_1_2pts = 0
        player_1_1pt = 0
        player_2_2pts = 0
        player_2_1pt = 0
        for i in range(match_length):
#            print("Game Number:", i)
            (board, qty) = init()
            player_1_code = 'W'
            player_2_code = 'B'
            r_seed = r.randint(1, 2)
            first_player = 'W' if r_seed == 1 else 'B'
            current_player = first_player
            if details:
                print(current_player, "goes first")
            while True:
                if qty[24] == 15:
                    if qty[25] == 0:
                        player_1_score += 2
                        player_1_2pts += 1
                        if details:
                            print(player_1, "wins by 2 points!")
                    else:
                        player_1_score += 1
                        player_1_1pt += 1
                        if details:
                            print(player_1, "wins by 1 point.")
                    break
                elif qty[25] == 15:
                    if qty[24] == 0:
                        player_2_score += 2
                        player_2_2pts += 1
                        if details:
                            print(player_2, "wins by 2 points!")
                    else:
                        player_2_score += 1
                        player_2_1pt += 1
                        if details:
                            print(player_2, "wins by 1 point.")
                    break
                else:
                    (roll_1, roll_2) = dice_roll()
                    (legal_full_moves, skip_filter) = move_finder(roll_1, roll_2, board, qty, current_player)
                    legal_full_moves = filtered_legal_moves(legal_full_moves, skip_filter, current_player, board, qty)
    #                print("Filtered Legal Moves:", legal_full_moves)
                    if current_player == player_1_code:
                        if player_1 == 'Random_Play':
                            chosen_move = engine_random_play(move_list = legal_full_moves)
                        elif player_1 == 'Cyrus_1.0':
                            (chosen_move, move_eval) = Cyrus_1_0(legal_full_moves, board, qty, current_player)
                        elif player_1 == 'Cyrus_1.1':
                            (chosen_move, move_eval) = Cyrus_1_1(legal_full_moves, board, qty, current_player)
                        elif player_1 == 'Cyrus_1.2':
                            (chosen_move, move_eval) = Cyrus_1_2(legal_full_moves, board, qty, current_player)
                        elif player_1 == 'Smart_Cyrus':
                            (chosen_move, move_eval) = Smart_Cyrus(legal_full_moves, board, qty, current_player, x_1,\
                                                                   y_1, z_1, a1_1, a2_1, a3_1, a4_1, a5_1, a6_1, a7_1,\
                                                                   a8_1, a9_1, a10_1)
                    else:
                        if player_2 == 'Random_Play':
                            chosen_move = engine_random_play(move_list = legal_full_moves)
                        elif player_2 == 'Cyrus_1.0':
                            (chosen_move, move_eval) = Cyrus_1_0(legal_full_moves, board, qty, current_player)
                        elif player_2 == 'Cyrus_1.1':
                            (chosen_move, move_eval) = Cyrus_1_1(legal_full_moves, board, qty, current_player)
                        elif player_2 == 'Cyrus_1.2':
                            (chosen_move, move_eval) = Cyrus_1_2(legal_full_moves, board, qty, current_player)
                        elif player_2 == 'Smart_Cyrus':
                            (chosen_move, move_eval) = Smart_Cyrus(legal_full_moves, board, qty, current_player, x_2,\
                                                                   y_2, z_2, a1_2, a2_2, a3_2, a4_2, a5_2, a6_2, a7_2,\
                                                                   a8_2, a9_2, a10_2)
                    if details:
                        print("Dice roll:", roll_1, roll_2)
                        print(current_player, "plays move:", chosen_move)
                        input("Press Enter to continue...")
                    (board, qty) = update_board_entire_move(board, qty, current_player, chosen_move)
                    current_player = 'W' if current_player == 'B' else 'B'
        scores.append([player_1_score, player_2_score])
        pt_margins.append([player_1_1pt, player_1_2pts, player_2_1pt, player_2_2pts])
    return (scores, pt_margins)


def main():
    print(cpu_match(player_1 = 'Cyrus_1.2', player_2 = 'Cyrus_1.0', match_length = 100, details = False, sets = 10))

#main()
