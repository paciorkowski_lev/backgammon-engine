"""
This module holds ALL heuristics and evaluation functions.

Status 04/19/2020: Currently supporting two heuristics:
    1) "Advancement" - a single number expressing how far away your pieces are from your goal. Lower is better
    2) "Vulnerability" - a composite score expressing how bad the gaps are in your position - explained below

Author: Lev Paciorkowski
Last updated: 12/10/2020
"""

# EXPORTS TO Engine_Core.py

# Ancillary function that returns True if any of a player's pieces exists before a given space (player's orientation)
def exists_before(player, space, board):
    range_start = 0 if player == 'W' else 12
    if player == 'W':
        range_end = space
    else:
        if space > 11:
            range_end = space
        else:
            range_end = space + 24
    for i in range(range_start, range_end):
        board_index = i-24 if i > 23 else i
        if board[board_index] == player:
            return True
    return False


# Ancillary function that returns True if any of a player's pieces exist after a given space (player's orientation)
def exists_after(player, space, board):
    if player == 'W':
        range_start = space + 1
    else:
        if space < 12:
            range_start = space + 25
        else:
            range_start = space + 1
    range_end = 24 if player == 'W' else 36
    for i in range(range_start, range_end):
        board_index = i-24 if i > 23 else i
        if board[board_index] == player:
            return True
    return False



# Function that finds all gaps between a player's pieces on the board
# Returns a list of lists: [[#, #], [#, #], ... ] each sublist [#, #] represents a gap: == [start, end]
# Note that having  start == end  is possible; that means the gap is of size 1
# All non-player occupied spaces on the board are either (1) the start of a gap, (2) the end of a gap, or (3) neither
# A space is (3)-neither if ANY of the following are true
#   (a) It is in between two non-player occupied spaces
#   (b) There are zero player-occupied spaces before the space (from player's perspective)
#   (c) There are zero player-occupied spaces after the space (from player's perspective) --> set proceed to 0
# A space is (2)-end if
#   (a) there is a player-occupied space immediately after the space
# A space is (1)-start if
#   (a) there is a player-occupied space immediately before the space
# Note that it is possible for a space to be BOTH (1) AND (2) - if (1) is TRUE, must check for (2) on that same space
def find_gaps(board, player):
    gaps = []
    range_start = 1 if player == 'W' else 13
    range_end = 23 if player == 'W' else 35
    gap_number = -1
    gap_action = 'opening'
    proceed = 1
    for i in range(range_start, range_end):
        if proceed == 0:
            break
        else:
            space = i-24 if i > 23 else i
            check_ahead = 0 if space == 23 else space + 1
            check_behind = 23 if space == 0 else space - 1
            if board[space] != player:
                if not exists_before(player, space, board):
                    continue
                elif not exists_after(player, space, board):
                    proceed = 0
                    continue
                elif board[check_ahead] != player and board[check_behind] != player:
                    continue
                else:
                    if gap_action == 'opening':
                        if board[check_ahead] == player and board[check_behind] == player:
                            gaps.append([space, space])
                        else:
                            gaps.append([space, None])
                            gap_action = 'closing'
                        gap_number += 1
                        continue
                    else:
                        gaps[gap_number][1] = space
                        gap_action = 'opening'
                        continue
            else:
                continue
    return gaps



# Function that returns the percent occupancy of a player's gap (occupancy meaning how much of the gap is filled by
#   opponent's pieces)
# An individual "gap" is a list of two elements: [a, b] where <a> is the start and <b> is the end of the gap,
#   referring to positions on the board
def gap_occupancy(gap, board, player):
    counter = 0
    opponent = 'W' if player == 'B' else 'B'
    if player == 'B' and gap[0] > 11 and gap[1] < 12:
        range_start = gap[0]
        range_end = gap[1] + 25
        for i in range(range_start, range_end):
            space = i - 24 if i > 23 else i
            if board[space] == 'W':
                counter += 1
        size = gap[1] - gap[0] + 25
        occupancy = counter / size
        return occupancy
    else:
        for i in range(gap[0], gap[1] + 1):
            if board[i] == opponent:
                counter += 1
        size = gap[1] - gap[0] + 1
        occupancy = counter / size
        return occupancy



# Function to calculate Advancement heuristic score for a position
def heur_advancement(board, qty, player):
    advancement = 0
    for i in range(24):
        if board[i] == player:
            if player == 'W':
                advancement += (-i + 24)*qty[i]
            else:
                if i < 12:
                    advancement += (-i + 12)*qty[i]
                else:
                    advancement += (-i + 36)*qty[i]
        else:
            continue
    return advancement


# Function to calculate Vulnerability heuristic score for a position
# It takes a list from the find_gaps(board, player) function to do this
def heur_vulnerability(board, player, gaps, size_multiplier, distance_multiplier, size_quad_a):
    vulnerability = 0
    if not gaps:
        return vulnerability
    else:
        if player == 'W':
            for gap in gaps:
                size = gap[1] - gap[0] + 1
                if size_multiplier == 'Quadratic':
                    size_addition = size_quad_a * size * size
                else:
                    size_addition = size * size_multiplier
                vulnerability += size_addition
 #               print("Size addition (white): ", size_addition, "\n", end="")
                distance = ((24 - gap[0]) + (24 - gap[1])) / 2
                vulnerability += distance * distance_multiplier
 #               print("Distance addition (white): ", distance * distance_multiplier, "\n", end="")
                occupancy = gap_occupancy(gap = gap, board = board, player = 'W')
                occupancy_multiplier = size_addition                                                               # GAP OCCUPANCY HEURISTIC -- ADJUSTABLE
                vulnerability += occupancy * occupancy_multiplier
 #               print("Occupancy addition (white): ", occupancy_multiplier * occupancy, "\n", end="")
            return vulnerability
        else:
            for gap in gaps:
                if gap[0] > 11 and gap[1] > 11:
                    size = gap[1] - gap[0] + 1
                    distance = ((36 - gap[0]) + (36 - gap[1])) / 2
                elif gap[0] > 11 and gap[1] < 12:
                    size = gap[1] - gap[0] + 25
                    distance = ((36 - gap[0]) + (12 - gap[1])) / 2
                else:
                    size = gap[1] - gap[0] + 1
                    distance = ((12 - gap[0]) + (12 - gap[1])) / 2
                occupancy = gap_occupancy(gap = gap, board = board, player = 'B')
                if size_multiplier == 'Quadratic':
                    size_addition = size_quad_a * size * size
                else:
                    size_addition = size * size_multiplier
                occupancy_multiplier = size_addition                                                               # GAP OCCUPANCY HEURISTIC -- ADJUSTABLE
                vulnerability += size_addition
                vulnerability += distance * distance_multiplier
                vulnerability += occupancy * occupancy_multiplier
  #              print("Size addition (black): ", size_addition, "\n", end="")
  #              print("Distance addition (black): ", distance * distance_multiplier, "\n", end="")
  #              print("Occupancy addition (black): ", occupancy * occupancy_multiplier, "\n", end="")
            return vulnerability



# parameterized version of advancement function that takes three general parameters x, y, z
def parameterized_advancement(board, qty, player, x, y, z):
    advancement = 0
    for i in range(24):
        if board[i] == player:
            if player == 'W':
                dist = (-i + 24)
                advancement += qty[i] * (x*(dist)**2 + y*(dist) + z)
            else:
                if i < 12:
                    dist = (-i + 12)
                    advancement += qty[i] * (x*(dist)**2 + y*(dist) + z)
                else:
                    dist = (-i + 36)
                    advancement += qty[i] * (x*(dist)**2 + y*(dist) + z)
        else:
            continue
    return advancement


# Parameterized version of vulnerability heuristic
def parameterized_vulnerability(board, player, gaps, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10):
    vulnerability = 0
    if not gaps:
        return vulnerability
    else:
        if player == 'W':
            for gap in gaps:
                size = gap[1] - gap[0] + 1
                distance = ((24 - gap[0]) + (24 - gap[1])) / 2
                occupancy = gap_occupancy(gap=gap, board=board, player='W')
                vuln_add = a1*(size**2) + a2*size + a3*(distance**2) + a4*distance + a5*(occupancy**2) +\
                    a6*occupancy + a7*size*distance + a8*size*occupancy + a9*distance*occupancy + a10
                vulnerability += vuln_add
            return vulnerability
        else:
            for gap in gaps:
                if gap[0] > 11 and gap[1] > 11:
                    size = gap[1] - gap[0] + 1
                    distance = ((36 - gap[0]) + (36 - gap[1])) / 2
                elif gap[0] > 11 and gap[1] < 12:
                    size = gap[1] - gap[0] + 25
                    distance = ((36 - gap[0]) + (12 - gap[1])) / 2
                else:
                    size = gap[1] - gap[0] + 1
                    distance = ((12 - gap[0]) + (12 - gap[1])) / 2
                occupancy = gap_occupancy(gap=gap, board=board, player='B')
                vuln_add = a1*(size**2) + a2*size + a3*(distance**2) + a4*distance + a5*(occupancy**2) +\
                    a6*occupancy + a7*size*distance + a8*size*occupancy + a9*distance*occupancy + a10
                vulnerability += vuln_add
            return vulnerability
