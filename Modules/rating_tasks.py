"""
Handles rating of matches to estimate playing strength of contestants
I closely modeled the rating system after the Chess ELO system
Random Play is defined to have a rating of 100, and a rating gap of 200 indicates an Expected "Margin" of roughly
    75% for the higher rated player
A player's "Margin" in a match is equal to the total number of points they scored divided by the total number of
    points scored in the match

For a player's first rated match, they will initially start as "Unrated". In that case, their rating after the match
    will be the rating for which their expected margin would be equal to the actual margin scored in the match - i.e.
    their rating will be determined as a comparison to their opponent's rating. This doesn't work if both players are
    unrated - matches where neither player has a rating will NOT be rated.

Note that if Player A plays a match against Player B and Player B is unrated but Player A isn't, Player A's rating
    will NOT change regardless of the match outcome. Player B's fresh rating will be determined as a comparison from
    Player A's rating.

Formulas used:
    [Expected Margin] = 1/(1 + 10^(-[Spread]/400))   where [Spread] = [Player's rating] - [Opponent's rating]
    [Spread] = -400 * log10(1/[Margin] - 1)          when solving for the expected Spread based on an actual margin
                                                        (used when the player is initially unrated)

When the players are both already rated, then use this formula to determine their new rating:
    [New Rating] = [Old Rating] + k*([Actual Margin] - [Expected Margin])*[Match Length]/50
        where [Expected Margin] is calculated with the formula above, [Match Length] is the number of games in the
        match, and k is a parameter determined as follows:
    GAMES               k
    < 1,000             20
    1,000 - 9,999       10
    10,000 - 99,999     5
    >= 100,000          3

    "GAMES" refers to the total number of rated games the player has ever played.

Here is a rough table showing how [Expected Margin] changes with [Spread]:
SPREAD      E(MARGIN)
0           50.00%
100         64.01%
200         75.97%
300         84.90%
400         90.91%
500         94.68%
600         96.93%
700         98.25%
1000        99.68%

Update 04/19/2020: Right now the ratings module is not fully operational; the current priority is optimizing
    heuristic parameters; the evaluation of an engine for now will not be done with formal ratings, but rather
    by its margin in standardized matches against a standardized opponent - e.g. Random Play or one of the basic
    versions Cyrus 1.0, Cyrus 1.1 or Cyrus 1.2.

Next steps: Currently this module is very primitive - ratings and  match logs are stored in txt files. A SQL server
    connection will be established to formally store data in a SQL table

Author: Lev Paciorkowski
Last edited: 04/19/2020
"""
from general_functions import *

@dataclass
class Player:
    name: str
    rating: any
    games: int
    k: int


# function to return the k-factor of a player given a number of games of played
def determine_k(games):
    if games < 1000:
        return 20
    elif games < 10000:
        return 10
    elif games < 100000:
        return 5
    else:
        return 3


# function to make an instance of a player
def make_player(name, rating, games):
    k = determine_k(games)
    posted_rating = "Unrated" if rating == "Unrated" else float(rating)
    player = Player(str(name), posted_rating, int(games), int(k))
    return player


# function to read a ratings file and extract needed information about players
# The filename will be "ratings.txt"
# Will return a dict of players with key of player name and value of dataclass "Player"
def read_ratings(filename):
    players = dict()
    lines = []
    for line in open(filename):
        info = line.replace(" ", "").strip().split("|")
        lines.append(info)
    for line in lines:
        if line[0] == "Rank" or line[0] == "="*49 or line[0] == "-"*49:
            continue
        else:
            name = str(line[1])
            rating = line[2]
            games = int(line[3])
            player = make_player(name, rating, games)
            players[name] = player
    return players

# function to aid in writing rating information to files
def space_filler(string, field_width):
    return field_width - len(string)



# Function for automated rating of official CPU matches
# ratings information is in "ratings.txt" and everything is logged in "log.txt"
def rate_match(player1, player2, total_score1, total_score2, num_games):
    players = read_ratings("ratings.txt")
    full_log = []
    for line in open("log.txt"):
        full_log.append(line)
    match_no = len(full_log) / 2
    p1_initial_rating = players[player1].rating
    p2_initial_rating = players[player2].rating
    p1_margin = round(total_score1 / (total_score1 + total_score2), 4)
    p2_margin = round(1 - p1_margin, 4)
    p1_k = players[player1].k
    p2_k = players[player2].k
    E_1 = "NULL" if (p2_initial_rating == "Unrated" or p1_initial_rating == "Unrated") else round(1 / (1 + 10**(-(p1_initial_rating - p2_initial_rating) / 400)), 4)
    E_2 = "NULL" if (p1_initial_rating == "Unrated" or p2_initial_rating == "Unrated") else round(1 / (1 + 10**(-(p2_initial_rating - p1_initial_rating) / 400)), 4)
    if p2_initial_rating == "Unrated":
        p1_rating_change = 0
    elif p1_initial_rating == "Unrated":
        p1_rating_change = -400 * m.log10(1/p1_margin - 1) + p2_initial_rating
    else:
        p1_rating_change = p1_k * (p1_margin - E_1) * num_games/50
    if p1_initial_rating == "Unrated":
        p2_rating_change = 0
    elif p2_initial_rating == "Unrated":
        p2_rating_change = -400 * m.log10(1/p2_margin - 1) + p1_initial_rating
    else:
        p2_rating_change = p2_k * (p2_margin - E_2) * num_games/50
    p1_rating_change = round(p1_rating_change, 2)
    p2_rating_change = round(p2_rating_change, 2)
    p1_new_rating = p1_rating_change if p1_initial_rating == "Unrated" else p1_initial_rating + p1_rating_change
    p2_new_rating = p2_rating_change if p2_initial_rating == "Unrated" else p2_initial_rating + p2_rating_change
    log = open("log.txt", "a")
    text =   str(match_no) + " "*space_filler(str(match_no), 10) + "|" +\
             str(num_games) + " "*space_filler(str(num_games), 10) + "|" +\
             str(player1) + " "*space_filler(player1, 15) + "|" +\
             str(player2) + " "*space_filler(player2, 15) + "|" +\
             str(p1_initial_rating) + " "*space_filler(str(p1_initial_rating), 17) + "|" +\
             str(p2_initial_rating) + " "*space_filler(str(p2_initial_rating), 17) + "|" +\
             str(p1_margin) + " "*space_filler(str(p1_margin), 9) + "|" +\
             str(p2_margin) + " "*space_filler(str(p2_margin), 9) + "|" +\
             str(p1_k) + " "*space_filler(str(p1_k), 4) + "|" +\
             str(p2_k) + " "*space_filler(str(p2_k), 4) + "|" +\
             str(E_1) + " "*space_filler(str(E_1), 8) + "|" +\
             str(E_2) + " "*space_filler(str(E_2), 8) + "|" +\
             str(p1_rating_change) + " "*space_filler(str(p1_rating_change), 16) + "|" +\
             str(p2_rating_change) + " "*space_filler(str(p2_rating_change), 16) + "|" +\
             str(p1_new_rating) + " "*space_filler(str(p1_new_rating), 13) + "|" +\
             str(p2_new_rating) + " "*space_filler(str(p2_new_rating), 13) + "\n" +\
             "-"*199 + "\n"
    log.write(text)
    log.close()
    players[player1].rating = p1_new_rating
    players[player1].games += num_games
    players[player1].k = determine_k(players[player1].games)
    players[player2].rating = p2_new_rating
    players[player2].games += num_games
    players[player2].k = determine_k(players[player2].games)
    ratings_list = []
    for key in players:
        rating_compare = -99999 if players[key].rating == "Unrated" else players[key].rating
        ratings_list.append([players[key].name, rating_compare])
    ratings_list.sort(key=lambda x: x[1], reverse=True)
    with open("ratings.txt", 'w') as write_file:
        write_file.write("Rank|Player         |Rating    |Games     |k   \n" +
                         "="*49 + "\n")
        for i in range(len(ratings_list)):
            rank = i+1
            player = ratings_list[i][0]
            rating = players[player].rating
            games = players[player].games
            k = players[player].k
            text =          str(rank) + " "*space_filler(str(rank), 4) + "|" +\
                             str(player) + " "*space_filler(str(player), 15) + "|" +\
                             str(rating) + " "*space_filler(str(rating), 10) + "|" +\
                             str(games) + " "*space_filler(str(games), 10) + "|" +\
                             str(k) + "\n" + "-"*49 + "\n"
            write_file.write(text)
        write_file.close()


