"""
Holds ad-hoc testing for debugging. This is an independent, standalone module.
One major bug that was fixed around March 2020 was the biasing of heuristic evaluation in favor of black

Author: Lev Paciorkowski
Last updated: 04/19/2020
"""
from move_finding import *
from graphics import *


test_board = ['W', 'B', 'W', 'W', 'W', 'B',
              'W',  0 , 'B',  0 , 'W',  0 ,
              'B', 'B', 'W', 'W', 'B', 'B',
              'B', 'W',  0 , 'B',  0 ,  0 ,  0, 0]
test_qty = [6, 1, 1, 1, 1, 1,
            1, 0, 2, 0, 1, 0,
            5, 1, 2, 1, 1, 1,
            2, 1, 0, 1, 0, 0,  0, 0]


# function to conduct a symmetry test - evaluate position from one side's viewpoint, then reverse everything and
# look at the reversed position from the opposite side's viewpoint --> objectively all evaluations should be the same
# and there should be the same number of legal moves; if not, then the symmetry test is failed
def symmetry_test(board, qty):
    (roll_1, roll_2) = dice_roll()
    print("The dice roll is:", roll_1, roll_2, end=" ")
    print("Evaulating from white's point of view ...")
    (legal_full_moves, skip_filter) = move_finder(roll_1, roll_2, board, qty, 'W')
    legal_full_moves = filtered_legal_moves(legal_full_moves, skip_filter, 'W', board, qty)
    print("Number of legal moves found:", len(legal_full_moves), "\n", end=" ")
    print("Legal move list:", "\n")
    print(legal_full_moves)
    (chosen_move, move_opp_advancement, move_opp_vulnerability,
                move_player_advancement, move_player_vulnerability) = Cyrus_1_2(legal_full_moves, board, qty, 'W')
    print("Move chosen by Cyrus 1.2: ", chosen_move, "\n", end="")
    print("Evaluations after chosen move (opp then player, advancement then vulnerability): ", move_opp_advancement,
          move_opp_vulnerability, move_player_advancement, move_player_vulnerability,"\n", end="")
    gaps = find_gaps(board, 'W')
    print("Gaps: ", gaps, "\n", end="")
    for gap in gaps:
        occupancy_score = gap_occupancy(gap, board, 'W')
        print("Occupancy score:", occupancy_score)
    print("Now flipping board state across lines of symmetry")
    flipped_board = []
    flipped_qty = []
    for i in range(26):
        if i < 24:
            assumed_position = i+12 if i < 12 else i-12
            if board[assumed_position] == 'W':
                flipped_board.append('B')
            elif board[assumed_position] == 'B':
                flipped_board.append('W')
            else:
                flipped_board.append(0)
            flipped_qty.append(qty[assumed_position])
        elif i == 24:
            if board[25] == 'B':
                flipped_board.append('W')
            else:
                flipped_board.append(0)
            flipped_qty.append(qty[25])
        else:
            if board[24] == 'W':
                flipped_board.append('B')
            else:
                flipped_board.append(0)
            flipped_qty.append(qty[24])
    print("Evaluating flipped position from black's point of view ...")
    (legal_full_moves, skip_filter) = move_finder(roll_1, roll_2, flipped_board, flipped_qty, 'B')
    legal_full_moves = filtered_legal_moves(legal_full_moves, skip_filter, 'B', flipped_board, flipped_qty)
    print("Number of legal moves found:", len(legal_full_moves), "\n", end=" ")
    print("Legal move list:", "\n")
    print(legal_full_moves)
    (chosen_move, move_opp_advancement, move_opp_vulnerability,
                move_player_advancement, move_player_vulnerability) = Cyrus_1_2(legal_full_moves, flipped_board, flipped_qty, 'B')
    print("Move chosen by Cyrus 1.2: ", chosen_move, "\n", end="")
    print("Evaluations after chosen move (opp then player, advancement then vulnerability): ", move_opp_advancement,
          move_opp_vulnerability, move_player_advancement, move_player_vulnerability,"\n", end="")
    gaps = find_gaps(flipped_board, 'B')
    print("Gaps: ", gaps, "\n", end="")
    for gap in gaps:
        occupancy_score = gap_occupancy(gap, flipped_board, 'B')
        print("Occupancy score:", occupancy_score)
    t.tracer(0, 0)
    draw_board()
    set_board_position(board=flipped_board, qty=flipped_qty)
    t.update()
    t.done()
