"""
This module is intended for human playing against Engines - a separate module exists to handle Engine vs. Engine
    matches.

Status 04/19/2020: Not too sophisticated, future developments will include a GUI with point and click actions

Author: Lev Paciorkowski
Last updated: 04/19/2020
"""
from graphics import *
from Engine_Core import *


# Core function for human vs. Engine gameplay
def play_game():
    (board, qty) = init()
    first_player_backend = str(input("Who will go first? Enter 'W' for white or 'B' for black: "))
    r_seed = r.randint(1,2)
    human_player_backend = 'W' if r_seed == 1 else 'B'
    cpu_player_backend = 'B' if r_seed == 1 else 'W'
    human_player_frontend = 'White' if human_player_backend == 'W' else 'Black'
    first_player_frontend = 'White' if first_player_backend == 'W' else 'Black'
    current_player_backend = first_player_backend
    while True:
        if qty[24] == 15:
            print("Game over! White wins.")
            break
        elif qty[25] == 15:
            print("Game over! Black wins.")
            break
        else:
            current_player_frontend = 'White' if current_player_backend == 'W' else 'Black'
            t.clearscreen()
            t.tracer(0, 0)
            draw_board()
            set_board_position(board = board, qty = qty)
            t.update()
            (roll_1, roll_2) = dice_roll()
            print(current_player_frontend, "to move; the dice roll is: ", roll_1, ",", roll_2)
            if current_player_frontend == human_player_frontend:
                m11 = int(input("First step start loc (99 for None): "))
                m12 = int(input("First step end loc (99 for None): "))
                m21 = int(input("Second step start loc (99 for None): "))
                m22 = int(input("Second step end loc (99 for None): "))
                m31 = int(input("Third step start loc (99 for None): "))
                m32 = int(input("Third step end loc (99 for None): "))
                m41 = int(input("Fourth step start loc (99 for None): "))
                m42 = int(input("Fourth step end loc (99 for None): "))
                m11 = m11 if m11 != 99 else None
                m12 = m12 if m12 != 99 else None
                m21 = m21 if m21 != 99 else None
                m22 = m22 if m22 != 99 else None
                m31 = m31 if m31 != 99 else None
                m32 = m32 if m32 != 99 else None
                m41 = m41 if m41 != 99 else None
                m42 = m42 if m42 != 99 else None
                human_move = [(m11, m12), (m21, m22), (m31, m32), (m41, m42)]
                (board, qty) = update_board_entire_move(board, qty, human_player_backend, human_move)
                current_player_backend = 'W' if current_player_backend == 'B' else 'B'
            else:
                (legal_full_moves, skip_filter) = move_finder(roll_1, roll_2, board, qty, cpu_player_backend)
                legal_full_moves = filtered_legal_moves(legal_full_moves, skip_filter, cpu_player_backend, board, qty)
                (chosen_move, move_eval) = Cyrus_1_0(move_list = legal_full_moves, board = board, qty = qty, player = cpu_player_backend)       # Change this line to adjust which engine to use
                print("Computer plays: ", chosen_move)
                print("Computer evaluation (>0 means cpu thinks it's better): ", move_eval)
                (board, qty) = update_board_entire_move(board, qty, cpu_player_backend, chosen_move)
                current_player_backend = 'W' if current_player_backend == 'B' else 'B'
