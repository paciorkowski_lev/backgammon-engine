"""
Deals with generating the list of legal moves, given some board state, a player and a dice roll
All game logic is here
Author: Lev Paciorkowski
Last updated: 12/10/2020
"""
from general_functions import *

# EXPORTS TO debugging.py, Engine_Core.py

# Simple function to determine if a player is in NORMAL play or ENDGAME play - returns TRUE if in endgame
#   and FALSE otherwise; ENDGAME play means ready to start throwing pieces away
def is_in_endgame(player, board):
    if player == 'W':
        for i in range(18):
            if board[i] == 'W':
                return False
        return True
    else:
        for i in range(12, 30):
            index = i-24 if i > 23 else i
            if board[index] == 'B':
                return False
        return True


# Function for determining if any of a player's pieces exist on the board before a certain space, for throwing away
def exists_priority_piece(player, space, board):
    endpoint = 23 if player == 'W' else 11
    for i in range(endpoint-5, space):
        if board[i] == player:
            return True
    return False


# Function that is given a single die roll as a step option, then returns all possible steps from board state
def step_choices(step, board, player):
    moves = []
    if is_in_endgame(player = player, board = board):
        endpoint = 23 if player == 'W' else 11
        goal = 24 if player == 'W' else 25
        for i in range(endpoint-5, endpoint+1):
            if board[i] == player:
                if step == endpoint + 1 - i:
                    moves += [(i, goal)]
                elif step > endpoint + 1 - i:
                    if not exists_priority_piece(player = player, space = i, board = board):
                        moves += [(i, goal)]
                    else:
                        continue
                else:
                    if board[i + step] == player or board[i + step] == 0:
                        moves += [(i, i + step)]
                    else:
                        continue
            else:
                continue
    else:
        range_start = 0 if player == 'W' else 12
        range_end = 24 if player == 'W' else 36
        for i in range(range_start, range_end):
            space = i-24 if i > 23 else i
            if board[space] == player:
                goto = space + step
                goto = goto - 24 if goto > 23 else goto
                if player == 'W' and goto < space:
                    continue
                elif player == 'B' and 6 <= space <= 11 and 12 <= goto <= 17:
                    continue
                elif board[goto] != 0 and board[goto] != player:
                    continue
                else:
                    moves += [(space, goto)]
            else:
                continue
    return moves


# Function for special case of rolling (3, 3), (4, 4) or (6, 6) on initial board state - legal moves will be hard
#   coded since it is the ONLY time a player is allowed to take two from their home space
def roll_is_special(roll_1, roll_2):
    if roll_1 == roll_2 == 3 or roll_1 == roll_2 == 4 or roll_1 == roll_2 == 6:
        return True
    else:
        return False


# Function to determine if a given player's piece exists past a certain space
def exists_opp_past_barrier(player, board, space):
    if player == 'B':
        for i in range(space+1, 24):
            if board[i] == 'W':
                return True
        return False
    else:
        if space >= 12:
            for i in range(space+1, 36):
                spot_test = i-24 if i > 23 else i
                if board[spot_test] == 'B':
                    return True
            return False
        else:
            for i in range(space+1, 12):
                if board[i] == 'B':
                    return True
            return False



# This function determines if a move creates an illegal barrier
def creates_illegal_barrier(move, board, qty, player):
    (new_board, new_qty) = update_board_entire_move(board, qty, player, move)
    range_start = 0 if player == 'B' else 12
    range_end = 24 if player == 'B' else 36
    endpoint = 23 if player == 'B' else 11
    counter = 0
    for i in range(range_start, range_end):
        space = i-24 if i > 23 else i
        if counter == 5:
            if space == endpoint and board[space] == player:
                return True
            else:
                if board[space] == player:
                    if exists_opp_past_barrier(player, board, space):
                        return False
                    else:
                        return True
                else:
                    counter = 0
        else:
            if new_board[space] == player:
                counter += 1
            else:
                counter = 0



# Function for updating board state given current board state, a player and a single move (using one die roll)
# Move is 2-tuple (from, to) --> denotes from index to index on board
def update_board_single_move(board, qty, player, move):
    updated_board = board[:]
    updated_qty = qty[:]
    initial = move[0]
    ending = move[1]
    if initial is None and ending is None:
        return (board, qty)
    else:
        updated_qty[initial] -= 1
        updated_qty[ending] += 1
        if updated_qty[initial] == 0:
            updated_board[initial] = 0
        if updated_qty[ending] == 1 and ending <= 23:
            updated_board[ending] = player
        return (updated_board, updated_qty)


# This function updates the board after a whole move - i.e. up to four single steps
# Move is the standard [(#, #), (#, #), (#, #), (#, #)]
def update_board_entire_move(board, qty, player, full_move):
    (new_board, new_qty) = update_board_single_move(board, qty, player, full_move[0])
    for i in range(1,4):
        (new_board, new_qty) = update_board_single_move(new_board, new_qty, player, full_move[i])
    return (new_board, new_qty)


# This function returns the "length" of a move - defined as the number of non-(None, None) steps in it
# move parameter is [(#, #), (#, #), (#, #), (#, #)] --> for example, consider the following move:
#   [(1, 4), (None, None), (None, None), (None, None)] --> this has length 1
def move_length(move):
    length = 0
    for i in range(4):
        if move[i][0] != None:
            length += 1
    return length


# Function returns True/False answer to whether a move takes too many pieces (>1) from home space
# Move parameter is [(#, #), (#, #), (#, #), (#, #)] --> a list of 4 2-tuples
def takes_too_many_from_home(move, player):
    home = 0 if player == 'W' else 12
    counter = 0
    for i in range(4):
        if move[i][0] == home:
            counter += 1
    if counter > 1:
        return True
    else:
        return False



# Function that takes a dice roll and board state and returns a list of ALL legal* moves available
#   legal* - will have to filter out moves that take two from home space and redundant moves that lead to the same
#   board state; there will be a separate function for this
# One individual legal move looks like this: [(#, #), (#, #), (#, #), (#, #)]
# The returned list will look like this: [[(#, #), (#, #), (#, #), (#, #)], [(#, #), (#, #), (#, #), (#, #)], ...]
# Will also return a special indicator to determine if the returned list needs to be edited or not --> to handle
#   exceptional cases involving taking multiple pieces from home space
# The last two (#, #) will be (None, None) unless the roll is a double - i.e. (3, 3) etc.
#   i.e. all four slots will only be used in case of doubles
# Distinguish between two modes of play: ENDGAME and NORMAL -- done in step_choice() function
def move_finder(roll_1, roll_2, board, qty, player):
    legal_full_moves = []
    (starting_board, starting_qty) = init()
    if roll_1 == roll_2:
        doubles = True
    else:
        doubles = False
    if board == starting_board and qty == starting_qty and roll_is_special(roll_1, roll_2):
        if player == 'W':
            if roll_1 == 3:
                legal_full_moves += [[(0, 9), (0, 3), (None, None), (None, None)]]
            elif roll_1 == 4:
                legal_full_moves += [[(0, 8), (0, 8), (None, None), (None, None)]]
            else:
                legal_full_moves += [[(0, 6), (0, 6), (None, None), (None, None)]]
        else:
            if roll_1 == 3:
                legal_full_moves += [[(12, 21), (12, 15), (None, None), (None, None)]]
            elif roll_1 == 4:
                legal_full_moves += [[(12, 20), (12, 20), (None, None), (None, None)]]
            else:
                legal_full_moves += [[(12, 18), (12, 18), (None, None), (None, None)]]
        skip_filter = True
        return (legal_full_moves, skip_filter)
    else:
        if not doubles:
            moves_1_1 = step_choices(step = roll_1, board = board, player = player)                                         # set of all steps with first roll from initial position for turn
            if not moves_1_1 == []:
                for i in range(0, len(moves_1_1)):
                    (updated_board, updated_qty) = update_board_single_move(board = board, qty = qty, player = player, move = moves_1_1[i])
                    moves_2_2 = step_choices(step = roll_2, board = updated_board, player = player)                         # set of steps with 2nd roll after each possible first roll step
                    if not moves_2_2 == []:
                        for j in range(0, len(moves_2_2)):
                            add_move = [[moves_1_1[i], moves_2_2[j], (None, None), (None, None)]]
                            legal_full_moves += add_move
                    add_move = [[moves_1_1[i], (None, None), (None, None), (None, None)]]                                   # Had this indented as an else block
                    legal_full_moves += add_move                                                                            #
            else:
                add_move = [[(None, None), (None, None), (None, None), (None, None)]]
                legal_full_moves += add_move
            moves_1_2 = step_choices(step = roll_2, board = board, player = player)                                        # set of all steps with second roll from initial position for turn
            if not moves_1_2 == []:
                for i in range(0, len(moves_1_2)):
                    (updated_board, updated_qty) = update_board_single_move(board = board, qty = qty, player = player, move = moves_1_2[i])
                    moves_2_1 = step_choices(step = roll_1, board = updated_board, player = player)                        # set of steps with 1st roll after each possible 2nd roll step
                    if not moves_2_1 == []:
                        for j in range(0, len(moves_2_1)):
                            add_move = [[moves_1_2[i], moves_2_1[j], (None, None), (None, None)]]
                            legal_full_moves += add_move
                    add_move = [[moves_1_2[i], (None, None), (None, None), (None, None)]]                                   # Had this indented as an else block
                    legal_full_moves += add_move                                                                            #
            else:
                add_move = [[(None, None), (None, None), (None, None), (None, None)]]
                legal_full_moves += add_move
        else:                                                                                                                 # case when rolled doubles
            moves_1 = step_choices(step = roll_1, board = board, player = player)
            if not moves_1 == []:
                for i in range(0, len(moves_1)):
                    (updated_board, updated_qty) = update_board_single_move(board = board, qty = qty, player = player, move = moves_1[i])
                    moves_2 = step_choices(step = roll_1, board = updated_board, player = player)
                    if not moves_2 == []:
                        for j in range(0, len(moves_2)):
                            (updated_board_2, updated_qty_2) = update_board_single_move(board = updated_board, qty = updated_qty, player = player, move = moves_2[j])
                            moves_3 = step_choices(step = roll_1, board = updated_board_2, player = player)
                            if not moves_3 == []:
                                for k in range(0, len(moves_3)):
                                    (updated_board_3, updated_qty_3) = update_board_single_move(board = updated_board_2, qty = updated_qty_2, player = player, move = moves_3[k])
                                    moves_4 = step_choices(step = roll_1, board = updated_board_3, player = player)
                                    if not moves_4 == []:
                                        for l in range(0, len(moves_4)):
                                            add_move = [[moves_1[i], moves_2[j], moves_3[k], moves_4[l]]]
                                            legal_full_moves += add_move
                                    add_move = [[moves_1[i], moves_2[j], moves_3[k], (None, None)]]                             # Had this indented as an else block
                                    legal_full_moves += add_move                                                                #
                            add_move = [[moves_1[i], moves_2[j], (None, None), (None, None)]]                                   # Had this indented as an else block
                            legal_full_moves += add_move                                                                        #
                    add_move = [[moves_1[i], (None, None), (None, None), (None, None)]]                                         # Had this indented as an else block
                    legal_full_moves += add_move                                                                                #
            else:
                add_move = [[(None, None), (None, None), (None, None), (None, None)]]
                legal_full_moves += add_move
        skip_filter = False
        return (legal_full_moves, skip_filter)


# Function to take the output from move_finder and filter out moves that take multiple pieces from home space (unless
#   it is one of the three exceptions) as well as redundant moves leading to the same board state, and additionally
#   invalid moves because they do not utilize all possible steps or create a barrier illegally (see rules)
# This takes the returns from move_finder()
def filtered_legal_moves(legal_full_moves, skip_filter, player, board, qty):
#    print("Unfiltered legal moves:", legal_full_moves)
    if skip_filter:
        return legal_full_moves
    else:
        legal_full_moves = [x for x in legal_full_moves if not takes_too_many_from_home(x, player)]                         # filters out moves that take >1 from home space
        move_lengths = []
#        print("Length of unfiltered legal_full_moves:", len(legal_full_moves))
        for i in range(len(legal_full_moves)):
            move_lengths.append(move_length(legal_full_moves[i]))
#        print("Move Lengths:", move_lengths)
        max_length = max(move_lengths)
        legal_full_moves = [x for x in legal_full_moves if move_length(x) == max_length]                                    # filters out moves that don't use all available steps
        boards = []
        qtys = []
        for i in range(len(legal_full_moves)):
            (new_board, new_qty) = update_board_entire_move(board, qty, player, legal_full_moves[i])
            boards += [new_board]
            qtys += [new_qty]
        for i in range(len(legal_full_moves)):
            if i+1 == len(legal_full_moves):
                continue
            else:
                for j in range(i+1, len(legal_full_moves)):
                    if boards[i] == boards[j] and qtys[i] == qtys[j]:
                        legal_full_moves[i] = 'REMOVE'
                        break
        legal_full_moves = [x for x in legal_full_moves if x != 'REMOVE']                                                   # filters out redundant moves
        legal_full_moves = [x for x in legal_full_moves if not creates_illegal_barrier(x, board, qty, player)]              # filters out moves that create an illegal barrier
        return legal_full_moves
