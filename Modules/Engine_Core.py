"""
This module holds the core functions for defining Engines. Engines can differ by the heuristics they use.
Eventually, engines may use neural networks for position evaluation.

Status 04/19/2020: Currently working on optimizing the Engine "Smart_Cyrus"

RULES:
- The board is an ordered sequence of 24 spaces; 2 players - white and black; 2 six-sided dice
- Each player starts with 15 pieces all stacked on their respective "home" space on the board;
    white's home space is position 0 and black's home space is position 12 (indexing from 0)
- OBJECTIVE: to throw away all pieces by traveling to the opposite end of the board; pieces can be thrown away once
    ALL 15 have collectively reached the last six spaces of the board (positions 19-24 for white, 6-11 for black) by
    a proper dice roll - e.g. white rolling 2 permits throwing away a piece from position 22; black rolling 6 permits
    throwing away a piece from position 6 (6 spaces away from black's goal)
- Special rule for throwing away: a dice roll of N permits throwing away from any position < N away from the end of
    the board, if and only if the player has NO pieces on all positions >= N away from end of board
- Normal play, turns: First, player throws the two dice. Then, they may make valid moves with 1 or 2 pieces according to
    the dice
- A move is valid if: (1) the distance between its start and end location is equal to the dice roll being used;
    (2) the end location is not occupied by opponent's piece
- Special rule: you CANNOT create a barrier of at least SIX consecutive own pieces unless AT LEAST ONE opponent's piece
    has already traveled past where the barrier is
- Dice rolls MUST be used individually, i.e. cannot just combine a roll of 6+3 into one move of 9; must be able to move
    3 first, then 6 or vice versa
- SPECIAL RULE, DOUBLES: Rolling double N+N yields four move options of N spaces each; can move up to 4 different
    pieces
- If you have a legal move, you MUST play it; if you can use both dice rolls, you MUST do so
- SPECIAL RULE, taking from home space: A maximum of 1 piece per turn may be removed from the home space, UNLESS on
    the first turn a player rolls doubles such that one piece would be unable to make all four moves; that is the ONLY
    exception though - this only happens if you roll (3, 3), (4, 4) or (6, 6) on your very first move

Author: Lev Paciorkowski
Last edited: 12/10/2020
"""
from move_finding import *
from heuristics import *

# EXPORTS TO cpu_playing.py, human_playing.py


# Most basic engine, random play --> Extremely weak, should lose almost every game by 2 points to skilled opposition
def engine_random_play(move_list):
    if not move_list:
        chosen_move = [(None, None), (None, None), (None, None), (None, None)]
    else:
        move_seed = r.randint(0, len(move_list) - 1)
        chosen_move = move_list[move_seed]
    return chosen_move


# More sophisticated engine that uses basic heuristics to evaluate positions and then choose a move
# Named after Cyrus, king of Ancient Persia
# Version 1.0 - Uses two core heuristics: "Advancement" and "Vulnerability"
# Advancement: Point-based scores allocated to each piece still on the board - 1 square away from goal is 1 pt,
#   2 squares away is 2pts, etc... At beginning of game, both players are at 360 points; lower score is better
#   Once advancement score = 0, then the player has won
# Vulnerability: Point-based score applied to each "gap" between a player's pieces; a "gap" is any set of consecutive
#   spaces not occupied by the player. Gaps have the following elements:
#                                                   1) Size - how many spaces wide the gaps is - larger is worse
#                                                   2) Proximity to opponent's pieces, by these stages:
#                                                       a) Fully occupied by opponent's pieces - worst possible
#                                                       b) Partially occupied - less bad
#                                                       c) Not occupied but pieces nearby *on a scale* - further away
#                                                          opponent's pieces are better
#                                                   3) Distance from goal - the farther the gap from your goal the
#                                                      worse
#                                                   * Gaps have to have pieces stuck behind them to be relevant
def Cyrus_1_0(move_list, board, qty, player):
    if not move_list:
        chosen_move = [(None, None), (None, None), (None, None), (None, None)]                                              # Added in to fix an unidentified bug
        move_eval = None
        return (chosen_move, move_eval)
    else:
        evals = []
        opponent = 'W' if player == 'B' else 'B'
        for move in move_list:
            (new_board, new_qty) = update_board_entire_move(board = board, qty = qty, player = player, full_move = move)
            player_advancement = heur_advancement(board = new_board, qty = new_qty, player = player)
            gaps_player = find_gaps(board = new_board, player = player)
            player_vulnerability = heur_vulnerability(board = new_board, player = player, gaps = gaps_player, size_multiplier = 10, distance_multiplier = 5, size_quad_a = None)
            opp_advancement = heur_advancement(board = new_board, qty = new_qty, player = opponent)
            opp_gaps = find_gaps(board = new_board, player = opponent)
            opp_vulnerability = heur_vulnerability(board = new_board, player = opponent, gaps = opp_gaps, size_multiplier = 10, distance_multiplier = 5, size_quad_a = None)
            eval = opp_advancement + opp_vulnerability - player_advancement - player_vulnerability
            evals.append(eval)
        choose_index = evals.index(max(evals))
        chosen_move = move_list[choose_index]
        move_eval = evals[choose_index]
        return (chosen_move, move_eval)


# Cyrus Version 1.1 --> decreases distance multiplier from Cyrus 1.0 (goes from 5 --> 2)
def Cyrus_1_1(move_list, board, qty, player):
    if not move_list:
        chosen_move = [(None, None), (None, None), (None, None), (None, None)]                                              # Added in to fix an unidentified bug
        move_eval = None
        return (chosen_move, move_eval)
    else:
        evals = []
        opponent = 'W' if player == 'B' else 'B'
        for move in move_list:
            (new_board, new_qty) = update_board_entire_move(board = board, qty = qty, player = player, full_move = move)
            player_advancement = heur_advancement(board = new_board, qty = new_qty, player = player)
            gaps_player = find_gaps(board = new_board, player = player)
            player_vulnerability = heur_vulnerability(board = new_board, player = player, gaps = gaps_player, size_multiplier = 10, distance_multiplier = 2, size_quad_a = None)
            opp_advancement = heur_advancement(board = new_board, qty = new_qty, player = opponent)
            opp_gaps = find_gaps(board = new_board, player = opponent)
            opp_vulnerability = heur_vulnerability(board = new_board, player = opponent, gaps = opp_gaps, size_multiplier = 10, distance_multiplier = 2, size_quad_a = None)
            eval = opp_advancement + opp_vulnerability - player_advancement - player_vulnerability
            evals.append(eval)
        choose_index = evals.index(max(evals))
        chosen_move = move_list[choose_index]
        move_eval = evals[choose_index]
        return (chosen_move, move_eval)

# Cyrus Version 1.2 --> increases size multiplier to a quadratic function of 3*(size)^2
def Cyrus_1_2(move_list, board, qty, player):
    if not move_list:
        chosen_move = [(None, None), (None, None), (None, None), (None, None)]                                              # Added in to fix an unidentified bug
        move_eval = None
        return (chosen_move, move_eval)
    else:
        evals = []
        opp_advancements = []
        opp_vulnerabilities = []
        player_advancements = []
        player_vulnerabilities = []
        opponent = 'W' if player == 'B' else 'B'
        for move in move_list:
            (new_board, new_qty) = update_board_entire_move(board = board, qty = qty, player = player, full_move = move)
            player_advancement = heur_advancement(board = new_board, qty = new_qty, player = player)
            gaps_player = find_gaps(board = new_board, player = player)
            player_vulnerability = heur_vulnerability(board = new_board, player = player, gaps = gaps_player, size_multiplier = 'Quadratic', distance_multiplier = 2, size_quad_a = 3)
            opp_advancement = heur_advancement(board = new_board, qty = new_qty, player = opponent)
            opp_gaps = find_gaps(board = new_board, player = opponent)
            opp_vulnerability = heur_vulnerability(board = new_board, player = opponent, gaps = opp_gaps, size_multiplier = 'Quadratic', distance_multiplier = 2, size_quad_a = 3)
            eval = opp_advancement + opp_vulnerability - player_advancement - player_vulnerability
            evals.append(eval)
            opp_advancements.append(opp_advancement)
            opp_vulnerabilities.append(opp_vulnerability)
            player_advancements.append(player_advancement)
            player_vulnerabilities.append(player_vulnerability)
        choose_index = evals.index(max(evals))
        chosen_move = move_list[choose_index]
        move_eval = evals[choose_index]
        return (chosen_move, move_eval)



# Attempt at optimizing an algorithm by iterated tweaking of parameters:
# Has 13 parameters - three attached to the advancement heuristic, and 10 for the vulnerability heuristic
def Smart_Cyrus(move_list, board, qty, player, x, y, z, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10):
    if not move_list:
        chosen_move = [(None, None), (None, None), (None, None), (None, None)]                                              # Added in to fix an unidentified bug
        move_eval = None
        return (chosen_move, move_eval)
    else:
        evals = []
        opponent = 'W' if player == 'B' else 'B'
        for move in move_list:
            (new_board, new_qty) = update_board_entire_move(board = board, qty = qty, player = player, full_move = move)
            player_advancement = parameterized_advancement(new_board, new_qty, player, x, y, z)
            gaps_player = find_gaps(board = new_board, player = player)
            player_vulnerability = parameterized_vulnerability(new_board, player, gaps_player, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10)
            opp_advancement = parameterized_advancement(new_board, new_qty, opponent, x, y, z)
            opp_gaps = find_gaps(board = new_board, player = opponent)
            opp_vulnerability = parameterized_vulnerability(new_board, opponent, opp_gaps, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10)
            eval = opp_advancement + opp_vulnerability - player_advancement - player_vulnerability
            evals.append(eval)
        choose_index = evals.index(max(evals))
        chosen_move = move_list[choose_index]
        move_eval = evals[choose_index]
        return (chosen_move, move_eval)

